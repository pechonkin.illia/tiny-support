#include <tinysupport/exception.hpp>

#include <tinysupport/assert.hpp>

#include <stdexcept>

namespace tiny::support {

std::string CurrentExceptionMessage() {
  auto current = std::current_exception();

  TINY_VERIFY(current, "Not in exception context");

  try {
    std::rethrow_exception(current);
  } catch (std::exception& e) {
    return {e.what()};
  } catch (...) {
    return "Unknown exception";
  }
}

}  // namespace tiny::support
