#include <tinysupport/result.hpp>

namespace tiny::support {

namespace make_result {

Status Ok() {
  return Status::Ok();
}

detail::Failure CurrentException() {
  return detail::Failure(std::current_exception());
}

detail::Failure Fail(std::error_code error) {
  TINY_VERIFY(error, "Expected error");
  return detail::Failure{error};
}

detail::Failure Fail(Error error) {
  TINY_VERIFY(error.HasError(), "Expected error");
  return detail::Failure(std::move(error));
}

Status ToStatus(std::error_code error) {
  if (error) {
    return Fail(error);
  } else {
    return Ok();
  }
}

}  // namespace make_result

}  // namespace tiny::support
