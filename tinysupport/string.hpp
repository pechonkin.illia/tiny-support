#pragma once

#include <ostream>
#include <string_view>

namespace tiny::support {

namespace detail {

struct Quoted {
  std::string_view str_;
};

std::ostream& operator<<(std::ostream& out, const detail::Quoted& q) {
  out << '\'' << q.str_ << '\'';
  return out;
}

}  // namespace detail

detail::Quoted Quoted(const std::string_view str) {
  return {str};
}

}  // namespace tiny::support
