#include <tinysupport/panic.hpp>

#include <iostream>

namespace tiny::support {
namespace detail {

void Panic(const std::string& error) {
  std::cerr << error << std::endl;
  std::abort();
}

}  // namespace detail
}  // namespace tiny::support
