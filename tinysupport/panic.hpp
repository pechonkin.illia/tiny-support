#pragma once

#include <tinysupport/string_builder.hpp>

namespace tiny::support {
namespace detail {
void Panic(const std::string& error);
}  // namespace detail
}  // namespace tiny::support

// Print error message to stderr, then abort
// Usage: TINY_PANIC("Internal error: " << e.what());

#define TINY_PANIC(error)                                       \
  do {                                                          \
    tiny::support::detail::Panic(tiny::support::StringBuilder() \
                                 << ": " << error);             \
  } while (false)
