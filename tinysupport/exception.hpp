#pragma once

#include <string>

namespace tiny::support {

std::string CurrentExceptionMessage();

}  // namespace tiny::support
