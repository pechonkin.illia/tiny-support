#include <gtest/gtest.h>

#include <tinysupport/string_builder.hpp>
#include <tinysupport/string.hpp>

#include <thread>

using namespace tiny::support;
using namespace std::chrono_literals;

TEST(StringUtils, Quoted) {
  std::string result = StringBuilder() << Quoted("Hello");
  ASSERT_EQ(result, "'Hello'");
}
