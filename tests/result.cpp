#include <gtest/gtest.h>

#include <tinysupport/result.hpp>

using namespace tiny::support;

////////////////////////////////////////////////////////////////////////////////

static std::error_code TimedOut() {
  return std::make_error_code(std::errc::timed_out);
}

////////////////////////////////////////////////////////////////////////////////

class TestClass {
 public:
  TestClass(std::string message)
    : message_(std::move(message)) {
    ++object_count_;
  }

  TestClass(const TestClass& that)
    : message_(that.message_) {
  }

  TestClass& operator =(const TestClass& that) {
    message_ = that.message_;
    return *this;
  }

  TestClass(TestClass&& that)
    : message_(std::move(that.message_)) {
  }

  TestClass& operator =(TestClass&& that) {
    message_ = std::move(that.message_);
    return *this;
  }

  ~TestClass() {
    --object_count_;
  }

  const std::string& Message() const {
    return message_;
  }

  static int ObjectCount() {
    return object_count_;
  }

 private:
  TestClass();

 private:
  std::string message_;
  static int object_count_;
};

int TestClass::object_count_ = 0;

////////////////////////////////////////////////////////////////////////////////

Result<std::vector<int>> MakeVector(size_t size) {
  std::vector<int> ints;
  ints.reserve(size);
  for (size_t i = 0; i < size; ++i) {
    ints.push_back(i);
  }
  return make_result::Ok(std::move(ints));
}

Result<std::string> MakeError() {
  return make_result::Fail(TimedOut());
}

////////////////////////////////////////////////////////////////////////////////

TEST(Result, Ok) {
  static const std::string kMessage = "Hello";

  auto result = Result<TestClass>::Ok(kMessage);
  ASSERT_TRUE(result.IsOk());
  ASSERT_TRUE(result);
  ASSERT_FALSE(result.HasError());
  result.ThrowIfError();  // Nothing happens
  result.ExpectOk();  // Nothing happens

  ASSERT_EQ(result.ValueUnsafe().Message(), kMessage);
  ASSERT_EQ((*result).Message(), "Hello");

  ASSERT_EQ(result->Message(), kMessage);
}

TEST(Result, ObjectCount) {
  {
    auto result = Result<TestClass>::Ok("Hi");
    ASSERT_EQ(TestClass::ObjectCount(), 1);
  }
  ASSERT_EQ(TestClass::ObjectCount(), 0);

  {
    auto result = Result<TestClass>::Fail(TimedOut());
    ASSERT_EQ(TestClass::ObjectCount(), 0);
  }
  ASSERT_EQ(TestClass::ObjectCount(), 0);
}

TEST(Result, OkCopyCtor) {
  static const std::string kMessage = "Copy me";

  auto result = Result<TestClass>::Ok(TestClass(kMessage));
  ASSERT_TRUE(result.IsOk());
  ASSERT_EQ(result->Message(), kMessage);
}

TEST(Result, Error) {
  auto result = Result<TestClass>::Fail(TimedOut());

  ASSERT_FALSE(result.IsOk());
  ASSERT_TRUE(!result);
  ASSERT_TRUE(result.HasError());

  auto error = result.GetErrorCode();
  ASSERT_EQ(error.value(), (int)std::errc::timed_out);

  ASSERT_THROW(result.ThrowIfError(), std::system_error);
}

TEST(Result, MatchErrorCode) {
  Result<void> result = make_result::Fail(TimedOut());
  ASSERT_TRUE(result.MatchErrorCode((int)std::errc::timed_out));
}

TEST(Result, Move) {
  auto result_1 = Result<TestClass>::Ok("Hello");
  auto result_2 = std::move(result_1);
  ASSERT_EQ(result_2.Value().Message(), "Hello");
}

TEST(Result, Copy) {
  auto result_1 = Result<TestClass>::Ok("Hello");
  Result<TestClass> result_2 = result_1;
  ASSERT_EQ(result_1.Value().Message(), "Hello");
}

TEST(Result, AccessMethods) {
  auto result = Result<TestClass>::Ok("Hello");
  ASSERT_EQ(result->Message(), "Hello");

  const TestClass& test = *result;
  ASSERT_EQ(test.Message(), "Hello");

  TestClass thief = std::move(*result);
  ASSERT_EQ(thief.Message(), "Hello");

  ASSERT_EQ(result.Value().Message(), "");
}

TEST(Result, Void) {
  // Ok
  auto result = Result<void>::Ok();
  ASSERT_FALSE(result.HasError());
  ASSERT_TRUE(result.IsOk());
  ASSERT_TRUE(result);
  result.ThrowIfError();  // Nothing happens
  result.ExpectOk();  // Nothing happens

  // Fail
  auto err_result = Result<void>::Fail(TimedOut());
  ASSERT_TRUE(err_result.HasError());
  ASSERT_TRUE(!err_result);
  ASSERT_THROW(err_result.ThrowIfError(), std::system_error);
  ASSERT_THROW(err_result.ExpectOk(), std::system_error);
  auto error = err_result.GetErrorCode();
  ASSERT_EQ(error.value(), (int)std::errc::timed_out);
}

TEST(Result, AutomaticallyUnwrapRvalue) {
  std::vector<int> ints = MakeVector(3);
  ASSERT_EQ(ints.size(), 3u);

  auto result_1 = MakeVector(4);
  ints = std::move(result_1);
  ASSERT_EQ(ints.size(), 4u);

  auto result_2 = MakeVector(5);
  // Does not compiled
  // ints = result_2;

  std::string str;
  ASSERT_THROW(str = MakeError(), std::system_error);
}

TEST(Result, MakeOkResult) {
  auto ok = make_result::Ok();
  ok.ExpectOk();
  ASSERT_TRUE(ok);

  const size_t answer = 4;
  Result<size_t> result = make_result::Ok(answer);
}

TEST(Result, MakeErrorResult) {
  Result<std::string> response = make_result::Fail(TimedOut());
  ASSERT_FALSE(response);

  Result<std::vector<std::string>> lines = make_result::PropagateError(response);
  ASSERT_FALSE(lines);
}

TEST(Result, MakeResultStatus) {
  auto result = make_result::ToStatus(TimedOut());
  ASSERT_FALSE(result);
  ASSERT_TRUE(result.HasError());
}

Result<int> IntResult(int value) {
  return make_result::Ok(value);
}

TEST(Result, Consistency) {
  auto result = IntResult(0);
  if (!result) {
    FAIL();
  }

  /*
  if (!IntResult(0)) {
    FAIL();
  }
  */
}

TEST(Result, JustStatus) {
  auto answer = Result<int>::Ok(42);
  auto ok = make_result::JustStatus(answer);
  ASSERT_TRUE(ok.IsOk());

  auto response = Result<std::string>::Fail(TimedOut());
  auto fail = make_result::JustStatus(response);
  ASSERT_FALSE(fail.IsOk());
}

TEST(Result, Exceptions) {
  auto bad = []() -> Result<std::string> {
    try {
      throw std::runtime_error("Bad");
      return make_result::Ok<std::string>("Good");
    } catch (...) {
      return make_result::CurrentException();
    }
  };

  auto result = bad();

  ASSERT_TRUE(result.HasError());

  auto error = result.GetError();
  ASSERT_TRUE(error.HasException());
  ASSERT_FALSE(error.HasErrorCode());

  ASSERT_THROW(result.ThrowIfError(), std::runtime_error);
}

TEST(Result, Invoke) {
  {
    auto good = []() -> int {
      return 42;
    };

    auto result = make_result::Invoke(good);
    ASSERT_TRUE(result.HasValue());
    ASSERT_EQ(*result, 42);
  }

  {
    auto bad = []() -> int {
      throw std::runtime_error("just test");
    };

    auto result = make_result::Invoke(bad);
    ASSERT_TRUE(result.HasError());
  }
}

TEST(Result, InvokeWithArguments) {
  auto sum = [](int x, int y) { return x + y; };
  auto result = make_result::Invoke(sum, 1, 2);
  ASSERT_EQ(*result, 3);
}

TEST(Result, Throw) {
  Result<int> result = make_result::Throw<std::runtime_error>("Test error");

  ASSERT_TRUE(result.HasError());
  ASSERT_THROW(result.ThrowIfError(), std::runtime_error);
}

struct MoveOnly {
  MoveOnly(int d) : data(d) {
  }

  MoveOnly(MoveOnly&& that) = default;
  MoveOnly& operator=(MoveOnly&& that) = default;

  MoveOnly(const MoveOnly& that) = delete;
  MoveOnly& operator=(const MoveOnly& that) = delete;

  int data;
};

TEST(Result, MoveOnly) {
  {
    auto r = Result<MoveOnly>::Ok(42);
    auto v = std::move(r).Value();
    ASSERT_EQ(v.data, 42);
  }

  {
    auto v = *(Result<MoveOnly>::Ok(17));
    ASSERT_EQ(v.data, 17);
  }
}

TEST(Result, InvokeMoveOnlyArguments) {
  auto foo = [](MoveOnly mo) {
    return MoveOnly(mo.data + 1);
  };

  auto result = make_result::Invoke(foo, MoveOnly(3));
  ASSERT_EQ(result->data, 4);
}
