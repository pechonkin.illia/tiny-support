#include <gtest/gtest.h>

#include <tinysupport/exception.hpp>

using namespace tiny::support;

TEST(Exception, RuntimeError) {
  try {
    throw std::runtime_error("test runtime error");
  } catch (...) {
    ASSERT_EQ(CurrentExceptionMessage(), "test runtime error");
  }
}
