#include <gtest/gtest.h>

#include <tinysupport/function.hpp>

#include <memory>
#include <optional>
#include <string>
#include <type_traits>

using namespace tiny::support;

TEST(Function, MoveOnlyLambda) {
  auto message_ref = std::make_unique<std::string>("Hello!");
  UniqueFunction<void()> f([message_ref = std::move(message_ref)]() {
    ASSERT_EQ(*message_ref, "Hello!");
  });
  auto g = std::move(f);
  g();
}

TEST(Function, CopyConstructible) {
  using F = UniqueFunction<void()>;
  ASSERT_FALSE(std::is_copy_constructible<F>::value);
}
