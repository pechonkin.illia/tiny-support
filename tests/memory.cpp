#include <gtest/gtest.h>

#include <tinysupport/memspan.hpp>
#include <tinysupport/mmap_allocation.hpp>

using tiny::support::MmapAllocation;
using tiny::support::MemSpan;

TEST(Memory, MmapAllocation) {
  auto alloc = MmapAllocation::AllocatePages(3);

  static const size_t kPageSize = 4096;

  ASSERT_EQ(alloc.Size(), kPageSize * 3);
  ASSERT_NE(alloc.Start(), (char *) 0);
  ASSERT_EQ(alloc.Start() + alloc.Size(), alloc.End());

  alloc.Release();
  alloc.Release();
}

TEST(Memory, ProtectPages) {
  auto alloc = MmapAllocation::AllocatePages(3);
  alloc.ProtectPages(1, 2);

  char* safe_addr = alloc.Start() + 256;
  *safe_addr = 17;

  auto write_to_protected_page = [&alloc]() {
    char* addr = alloc.Start() + 4096 * 2;
    *addr = 42;
  };

  auto read_from_protected_page = [&alloc]() {
    char* addr = alloc.Start() + 4096 * 2;
    char byte = *addr;
  };

  // SIGSEGV, SIGBUS on Apple?
  static const int kExpectedSignal = SIGBUS;

  EXPECT_EXIT(write_to_protected_page(), ::testing::KilledBySignal(kExpectedSignal), ".*");
  EXPECT_EXIT(read_from_protected_page(), ::testing::KilledBySignal(kExpectedSignal), ".*");
}

TEST(Memory, MemSpan) {
  auto alloc = MmapAllocation::AllocatePages(3);

  auto span = alloc.AsMemSpan();

  ASSERT_EQ(span.Size(), alloc.Size());
  ASSERT_EQ(span.Data(), alloc.Start());

  ASSERT_EQ(span.Begin(), span.Data());
  ASSERT_EQ(span.Begin() + span.Size(), span.End());
}
