#include <gtest/gtest.h>

#include <tinysupport/string_builder.hpp>

using namespace tiny::support;

TEST(StringBuilder, HelloWorld) {
  std::string greeting = StringBuilder() << "Hello" << ", " << "World" << "!";
  ASSERT_EQ(greeting, "Hello, World!");
}

TEST(StringBuilder, NotOnlyStrings) {
  char chr = 'X';
  int answer = 42;
  bool locked = true;

  std::string result = StringBuilder() << "char = " << chr
                                       << ", answer = " << answer << ", locked = " << locked;

  // TODO: booleans
  ASSERT_EQ(result, "char = X, answer = 42, locked = 1");
}